﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquirrelScript : MonoBehaviour {

    private BoxCollider2D Squirrel;

    // Use this for initialization
    void Start()
    {
        Squirrel = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        //if (GameScript.result || GameTwoScript.result)
        //{
        //    RepositionObject();
        //}
    }

    public void RepositionObject()
    {
        Vector2 groundOffSet = new Vector2(125f, 0);
        Squirrel.transform.position = (Vector2)Squirrel.transform.position + groundOffSet;
    }
}
