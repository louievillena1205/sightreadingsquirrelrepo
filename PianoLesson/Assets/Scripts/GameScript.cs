﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScript : MonoBehaviour {

    public Canvas PopUpCanvas;
    public Canvas FailPopUpCanvas;
    public Canvas NextLevelCanvas;
    public int score = 0;
    public string pianoText;
    public string answer;
    public string keyword;
    public static bool result;
    public Text ResultText;
    public Text ModalResultText;
    public Image Note;

    public SquirrelScript squirrel;

    // Use this for initialization
    void Start () {
        PopUpCanvas.enabled = false;
        FailPopUpCanvas.enabled = false;
        NextLevelCanvas.enabled = false;
        result = false;
        GenerateNote(MenuScript.gameQuestion);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void TryAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PianoPressed(string key)
    {
        squirrel.RepositionObject();
        if (pianoText.Length < 8)
        {
            pianoText = pianoText + key;
            //ResultText.text = pianoText;
        }

        if (pianoText.Length == 8)
        {
            if (pianoText == answer)
            {
                pianoText = "";
                //ResultText.text = "CORRECT";
                MenuScript.gameQuestion++;
                if (MenuScript.gameQuestion > 9)
                {
                    NextLevelCanvas.enabled = true;
                } else
                {
                    PopUpCanvas.enabled = true;
                }
            } else
            {
                pianoText = "";
                //ResultText.text = "WRONG";
                FailPopUpCanvas.enabled = true;
            }
        }
    }

    public void GenerateNote(int number)
    {
        if (number == 1)
        {
            Note.sprite = Resources.Load<Sprite>("notes/BELOW-C/ACAC");
            answer = "BABCBABC";
            //keyword = "B";
            //pianoText = keyword;
        }
        else if (number == 2)
        {
            Note.sprite = Resources.Load<Sprite>("notes/BELOW-C/AFAF");
            answer = "BABFBABF";
            //keyword = "B";
            //pianoText = keyword;
        }
        else if (number == 3)
        {
            Note.sprite = Resources.Load<Sprite>("notes/BELOW-C/BABA");
            answer = "BBBABBBA";
            //keyword = "B";
            //pianoText = keyword;
        }
        else if (number == 4)
        {
            Note.sprite = Resources.Load<Sprite>("notes/BELOW-C/CBAB");
            answer = "BCBBBABB";
            //keyword = "B";
            //pianoText = keyword;
        }
        else if (number == 5)
        {
            Note.sprite = Resources.Load<Sprite>("notes/BELOW-C/FBAG");
            answer = "BFBBBABG";
            //keyword = "B";
            //pianoText = keyword;
        }
        else if (number == 6)
        {
            Note.sprite = Resources.Load<Sprite>("notes/BELOW-C/FGFB");
            answer = "BFBGBFBB";
            //keyword = "B";
            //pianoText = keyword;
        }
        else if (number == 7)
        {
            Note.sprite = Resources.Load<Sprite>("notes/ABOVE-C/CDED");
            answer = "ACADAEAD";
            //keyword = "A";
            //pianoText = keyword;
        }
        else if (number == 8)
        {
            Note.sprite = Resources.Load<Sprite>("notes/ABOVE-C/DFEF");
            answer = "ADAFAEAF";
            //keyword = "A";
            //pianoText = keyword;
        }
        else if (number == 9)
        {
            Note.sprite = Resources.Load<Sprite>("notes/ABOVE-C/FDGE");
            answer = "AFADAGAE";
            //keyword = "A";
            //pianoText = keyword;
        }
    }
}
