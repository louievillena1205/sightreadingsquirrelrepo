﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameTwoScript : MonoBehaviour
{

    public Canvas PopUpCanvas;
    public Canvas FailPopUpCanvas;
    public Canvas NextLevelCanvas;
    public int score = 0;
    public string pianoText;
    public string answer;
    public string keyword;
    public static bool result;
    public Text ResultText;
    public Text ModalResultText;
    public Image Note;

    public SquirrelScript squirrel;

    // Use this for initialization
    void Start()
    {
        PopUpCanvas.enabled = false;
        FailPopUpCanvas.enabled = false;
        NextLevelCanvas.enabled = false;
        result = false;
        GenerateNote(MenuScript.gameTwoQuestion);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GoToScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void TryAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PianoPressed(string key)
    {
        squirrel.RepositionObject();
        if (pianoText.Length < 8)
        {
            pianoText = pianoText + key;
            //ResultText.text = pianoText;
        }

        if (pianoText.Length == 8)
        {
            if (pianoText == answer)
            {
                pianoText = "";
                //ResultText.text = "CORRECT";
                MenuScript.gameTwoQuestion++;
                if (MenuScript.gameTwoQuestion > 11)
                {
                    NextLevelCanvas.enabled = true;
                }
                else
                {
                    PopUpCanvas.enabled = true;
                }
            }
            else
            {
                pianoText = "";
                //ResultText.text = "WRONG";
                FailPopUpCanvas.enabled = true;
            }
        }
    }

    public void GenerateNote(int number)
    {
        if (number == 1)
        {
            Note.sprite = Resources.Load<Sprite>("notes/LEVEL2/CBAG");
            answer = "LCLBLALG";
            //keyword = "L";
            //pianoText = keyword;
        }
        else if (number == 2)
        {
            Note.sprite = Resources.Load<Sprite>("notes/LEVEL2/DECC");
            answer = "LDLELCLC";
            //keyword = "L";
            //pianoText = keyword;
        }
        else if (number == 3)
        {
            Note.sprite = Resources.Load<Sprite>("notes/LEVEL2/ECDF");
            answer = "LELCLDLF";
            //keyword = "L";
            //pianoText = keyword;
        }
        else if (number == 4)
        {
            Note.sprite = Resources.Load<Sprite>("notes/LEVEL2/GABA");
            answer = "LGLALBLA";
            //keyword = "L";
            //pianoText = keyword;
        }
        else if (number == 5)
        {
            Note.sprite = Resources.Load<Sprite>("notes/TREBLE-C/GFEC");
            answer = "TGTFTETC";
            //keyword = "T";
            //pianoText = keyword;
        }
        else if (number == 6)
        {
            Note.sprite = Resources.Load<Sprite>("notes/TREBLE-C/ACBC");
            answer = "TATCTBTC";
            //keyword = "T";
            //pianoText = keyword;
        }
        else if (number == 7)
        {
            Note.sprite = Resources.Load<Sprite>("notes/TREBLE-C/CDED");
            answer = "TCTDTETD";
            //keyword = "T";
            //pianoText = keyword;
        }
        else if (number == 8)
        {
            Note.sprite = Resources.Load<Sprite>("notes/TREBLE-C/DFDF");
            answer = "TDTFTDTF";
            //keyword = "T";
            //pianoText = keyword;
        }
        else if (number == 9)
        {
            Note.sprite = Resources.Load<Sprite>("notes/TREBLE-C/EFCD");
            answer = "TETFTCTD";
            //keyword = "T";
            //pianoText = keyword;
        }
        else if (number == 10)
        {
            Note.sprite = Resources.Load<Sprite>("notes/TREBLE-C/GBBG");
            answer = "TGTBTBTG";
            //keyword = "T";
            //pianoText = keyword;
        }
        else if (number == 11)
        {
            Note.sprite = Resources.Load<Sprite>("notes/TREBLE-C/GECD");
            answer = "TGTETCTD";
            //keyword = "T";
            //pianoText = keyword;
        }
    }
}
