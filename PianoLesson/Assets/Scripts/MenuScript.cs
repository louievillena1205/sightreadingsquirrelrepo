﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

    public static int gameQuestion = 1;
    public static int gameTwoQuestion = 1;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
